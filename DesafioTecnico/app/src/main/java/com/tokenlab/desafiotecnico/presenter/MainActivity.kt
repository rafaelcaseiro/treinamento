package com.tokenlab.desafiotecnico.presenter

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.databinding.ActivityMainBinding
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerFragment
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.MovieListViewFluxContainer
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.SecondaryViewFluxContainer

class MainActivity : AppCompatActivity() {
    companion object {
        private const val movieListFluxTag: String = "movieListFluxTag"
        private const val secondaryFluxTag: String = "secondaryFluxTag"
    }

    private var activeFragmentTag: String? = movieListFluxTag

    private val movieListViewFluxContainer: MovieListViewFluxContainer by lazy {
        supportFragmentManager.findFragmentByTag(movieListFluxTag) as? MovieListViewFluxContainer
            ?: MovieListViewFluxContainer.newInstance()
    }

    private val secondaryViewFluxContainer: SecondaryViewFluxContainer by lazy {
        supportFragmentManager.findFragmentByTag(secondaryFluxTag) as? SecondaryViewFluxContainer
            ?: SecondaryViewFluxContainer.newInstance()
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        setupNavActions()

        supportFragmentManager.beginTransaction().hide(secondaryViewFluxContainer)
            .show(movieListViewFluxContainer).commit()

        activeFragmentTag = movieListViewFluxContainer.tag
    }

    /**
     * Redirect the [onBackPressed] call to the active [FluxContainerFragment].
     */
    override fun onBackPressed() {
        val currentFragment: FluxContainerFragment =
            supportFragmentManager.findFragmentByTag(activeFragmentTag) as FluxContainerFragment

        currentFragment.onBackPressed()
    }

    private fun setupNavActions() {
        supportFragmentManager.beginTransaction()
            .add(R.id.main_flux_container, movieListViewFluxContainer, movieListFluxTag)
            .add(R.id.main_flux_container, secondaryViewFluxContainer, secondaryFluxTag)
            .commitNow()

        //Cliques na bottomnavigation
        binding.bottomNavigationView.setOnItemSelectedListener { onNavigationItemSelected(it) }
    }

    private fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_movielist -> {
                supportFragmentManager.beginTransaction().hide(secondaryViewFluxContainer)
                    .show(movieListViewFluxContainer).commit()
                activeFragmentTag = movieListViewFluxContainer.tag
                true
            }
            R.id.navigation_secondary -> {
                supportFragmentManager.beginTransaction().hide(movieListViewFluxContainer)
                    .show(secondaryViewFluxContainer).commit()
                activeFragmentTag = secondaryViewFluxContainer.tag
                true
            }
            else -> {
                false
            }
        }
    }
}