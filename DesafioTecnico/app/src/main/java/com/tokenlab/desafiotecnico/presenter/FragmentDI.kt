package com.tokenlab.desafiotecnico.presenter

// Projeto de unificação das DIs dos fragments
/*
@Module
class FragmentModule {
    @PerScreen
    @Provides
    fun getFluxContainerComponent(parentFragment: Fragment): FluxContainerComponent =
        DaggerFragmentComponent
            .builder()
            .fluxContainerComponent((parentFragment as? FluxContainerFragment)?.fluxContainerComponent)
            .build()
}

@PerScreen
@Component(dependencies = [FluxContainerComponent::class])
interface FragmentComponent {
    fun inject(movieListFragment: MovieListFragment)
    fun inject(movieDetailsFragment: MovieDetailsFragment)
    fun inject(secondaryOneFragment: SecondaryOneFragment)
    fun inject(secondaryTwoFragment: SecondaryTwoFragment)
}
// */
