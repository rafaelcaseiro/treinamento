package com.tokenlab.desafiotecnico.presenter

/**
 * Interface that marks the initial screen to each flux on the bottom navigation bar.
 */
interface InitialScreen