package com.tokenlab.desafiotecnico.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tokenlab.desafiotecnico.domain.model.Movie

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieListDao(): MovieDao
}
