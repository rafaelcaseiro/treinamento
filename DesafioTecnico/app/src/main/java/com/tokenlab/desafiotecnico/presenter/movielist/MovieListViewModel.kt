package com.tokenlab.desafiotecnico.presenter.movielist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.tokenlab.desafiotecnico.MovieApplication
import com.tokenlab.desafiotecnico.data.ApiStatus
import com.tokenlab.desafiotecnico.data.DaggerDataServicesComponent
import com.tokenlab.desafiotecnico.domain.model.Movie
import com.tokenlab.desafiotecnico.domain.repository.MovieRepository
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolder
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolderDelegate
import com.tokenlab.desafiotecnico.util.Resource
import javax.inject.Inject

class MovieListViewModel : ViewModel(), DisposableHolder by DisposableHolderDelegate() {

    //Injecting the repository that will handle data management
    @Inject
    lateinit var repository: MovieRepository

    // The liveData holding the list of movies to be exposed on the recycler view
    val liveDataMovieResource by lazy { repository.getMovieList().asLiveData() }

    // The internal MutableLiveData that stores the liveDataApiStatus of the most recent request
    private val _mutableApiStatus = MutableLiveData<ApiStatus>()

    // The external immutable LiveData for the request liveDataApiStatus, which exposes it to the binding adapter
    val liveDataApiStatus: LiveData<ApiStatus>
        get() {
            if (liveDataMovieResource.value is Resource.Loading &&
                (liveDataMovieResource.value as Resource.Loading<List<Movie>>).data.isNullOrEmpty()
            )
                _mutableApiStatus.value = ApiStatus.LOADING
            else if (liveDataMovieResource.value is Resource.Error &&
                (liveDataMovieResource.value as Resource.Error<List<Movie>>).data.isNullOrEmpty()
            )
                _mutableApiStatus.value = ApiStatus.ERROR
            else _mutableApiStatus.value = ApiStatus.DONE

            return _mutableApiStatus
        }

    // The internal MutableLiveData that stores the id for the most recent Movie selected
    private val _liveDataSelectedMovieId = MutableLiveData<Int?>()

    // The external immutable LiveData for the id of the selected Movie
    val liveDataSelectedMovieId: LiveData<Int?>
        get() = _liveDataSelectedMovieId

    init {
        DaggerDataServicesComponent.builder()
            .movieApplicationComponent(MovieApplication.instance.component)
            .build()
            .inject(this@MovieListViewModel)
    }

    /**
     * When the property is clicked, set the [_liveDataSelectedMovieId] [MutableLiveData]
     * @param movieId the id for the [Movie] that was clicked on.
     */
    fun displayMovieDetails(movieId: Int) {
        _liveDataSelectedMovieId.value = movieId
    }

    /**
     * After the navigation has taken place, make sure [liveDataSelectedMovieId] is set to null
     */
    fun displayMovieDetailsComplete() {
        _liveDataSelectedMovieId.value = null
    }
}