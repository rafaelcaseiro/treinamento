package com.tokenlab.desafiotecnico.data

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tokenlab.desafiotecnico.MovieApplicationComponent
import com.tokenlab.desafiotecnico.presenter.moviedetails.MovieDetailsViewModel
import com.tokenlab.desafiotecnico.presenter.movielist.MovieListViewModel
import com.tokenlab.desafiotecnico.util.PerService
import dagger.Component
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

const val BASE_URL = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"

@Module
class ApiServiceModule {
    @PerService
    @Provides
    fun getTmdbApiService(retrofit: Retrofit): TmdbApiService =
        retrofit.create(TmdbApiService::class.java)

    @Provides
    fun getRetrofit(retrofitBuilder: Retrofit.Builder): Retrofit =
        retrofitBuilder.baseUrl(BASE_URL)
            .addCallAdapterFactory(
                RxJava3CallAdapterFactory.create()
            ).build()

    @Provides
    fun getRetrofitBuilder(moshi: Moshi): Retrofit.Builder =
        Retrofit.Builder().addConverterFactory(MoshiConverterFactory.create(moshi))

    @Provides
    fun getMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

@PerService
@Component(dependencies = [(MovieApplicationComponent::class)],
    modules = [ApiServiceModule::class])
interface DataServicesComponent {
    fun inject(movieListViewModel: MovieListViewModel)
    fun inject(movieDetailsViewModel: MovieDetailsViewModel)
}