package com.tokenlab.desafiotecnico.util

import android.util.Log
import kotlinx.coroutines.flow.*

inline fun <ResultType, RequestType> networkBoundResource(
    crossinline query: () -> Flow<ResultType>,
    crossinline fetch: suspend () -> RequestType,
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
    crossinline shouldFetch: (ResultType) -> Boolean = { true },
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data)) {
        emit(Resource.Loading(data))
        try {
            saveFetchResult( fetch() )
            query().map { Resource.Success(it) }
        } catch (throwable: Throwable) {
            query().map {
                Log.i("NetworkBoundResource",
                    "Fetch unsuccessful, returned with exception $throwable.")
                Resource.Error(throwable, it)
            }
        }
    } else {
        query().map { Resource.Success(it) }
    }

    emitAll(flow)
}