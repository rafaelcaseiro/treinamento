package com.tokenlab.desafiotecnico.util

import javax.inject.Scope

@Scope
annotation class PerFlux

@Scope
annotation class PerScreen

@Scope
annotation class PerService