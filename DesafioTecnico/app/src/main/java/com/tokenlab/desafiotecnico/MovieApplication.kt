package com.tokenlab.desafiotecnico

import android.app.Application

class MovieApplication : Application() {
    companion object {
        lateinit var instance: MovieApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val component: MovieApplicationComponent by lazy {
        DaggerMovieApplicationComponent
            .builder()
            .movieApplicationModule(MovieApplicationModule(this))
            .build()
    }
}