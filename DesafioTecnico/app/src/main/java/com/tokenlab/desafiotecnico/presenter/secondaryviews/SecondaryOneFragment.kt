package com.tokenlab.desafiotecnico.presenter.secondaryviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.databinding.FragmentSecondaryOneBinding
import com.tokenlab.desafiotecnico.presenter.InitialScreen
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.*
import javax.inject.Inject

class SecondaryOneFragment : Fragment(), BackButtonListener, InitialScreen,
    DisposableHolder by DisposableHolderDelegate() {
    @Inject
    lateinit var cicerone: Cicerone<Router>

    private val secondaryOneComponent: SecondaryOneComponent by lazy {
        DaggerSecondaryOneComponent
            .builder()
            .fluxContainerComponent((parentFragment as? FluxContainerFragment)?.fluxContainerComponent)
            .build()
    }

    companion object {
        fun newInstance(): SecondaryOneFragment = SecondaryOneFragment()
    }

    private lateinit var binding: FragmentSecondaryOneBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentSecondaryOneBinding.inflate(layoutInflater, container, false)

        secondaryOneComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val button: Button = view.findViewById(R.id.button)

        button.setOnClickListener {
            cicerone.router.navigateTo(FragmentSecondaryTwoScreen)
        }
    }

    override fun onBackPressed(): Boolean {
        cicerone.router.exit()
        return true
    }

    override fun onDestroy() {
        disposeAll()
        super.onDestroy()
    }
}