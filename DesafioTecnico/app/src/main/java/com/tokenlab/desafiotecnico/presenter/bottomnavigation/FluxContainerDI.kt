package com.tokenlab.desafiotecnico.presenter.bottomnavigation

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Navigator
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.util.PerFlux
import dagger.Component
import dagger.Module
import dagger.Provides

@Module
class FluxContainerModule(
    private val frameActivity: FragmentActivity,
    private val fragmentManager: FragmentManager,
) {

    @Provides
    @PerFlux
    fun getNavigator(): Navigator {
        return AppNavigator(frameActivity, R.id.flux_container, fragmentManager)
    }

    @Provides
    @PerFlux
    fun getCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    @PerFlux
    fun getNavigationHolder(cicerone: Cicerone<Router>): NavigatorHolder =
        cicerone.getNavigatorHolder()

    @Provides
    @PerFlux
    fun getRouter(cicerone: Cicerone<Router>): Router = cicerone.router
}

@Component(modules = [FluxContainerModule::class])
@PerFlux
interface FluxContainerComponent {
    fun getCicerone(): Cicerone<Router>
    fun getNavigationHolder(): NavigatorHolder
    fun getRouter(): Router
    fun inject(fluxContainerFragment: FluxContainerFragment)
}
