package com.tokenlab.desafiotecnico.presenter.moviedetails

import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerComponent
import com.tokenlab.desafiotecnico.util.PerScreen
import dagger.Component

@PerScreen
@Component(dependencies = [FluxContainerComponent::class])
interface MovieDetailsComponent {
    fun inject(movieDetailsFragment: MovieDetailsFragment)
}