package com.tokenlab.desafiotecnico.data

import androidx.room.withTransaction
import com.tokenlab.desafiotecnico.domain.model.Movie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CacheDataSource @Inject constructor(private val db: MovieDatabase) {
    private val movieListDao = db.movieListDao()

    fun getMovieList(): Flow<List<Movie>> {
        return movieListDao.getMovieList()
    }

    suspend fun updateMovieList(movieList: List<Movie>) {
        db.withTransaction {
            movieListDao.deleteMovieList()
            movieListDao.insertMovieList(movieList)
        }
    }

    fun getMovieDetails(movieId: Int): Flow<Movie> {
        return movieListDao.getMovieDetails(movieId)
    }

    suspend fun updateMovieDetails(movie: Movie) {
        movieListDao.insertMovie(movie)
    }
}