package com.tokenlab.desafiotecnico.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tokenlab.desafiotecnico.domain.model.Movie
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie_list ORDER BY voteAverage DESC")
    fun getMovieList(): Flow<List<Movie>>

    @Query("SELECT * FROM movie_list WHERE id = :id")
    fun getMovieDetails(id: Int): Flow<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieList(movieList: List<Movie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: Movie)

    @Query("DELETE FROM movie_list")
    suspend fun deleteMovieList()
}