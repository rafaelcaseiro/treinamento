package com.tokenlab.desafiotecnico.domain.repository

import com.tokenlab.desafiotecnico.data.CacheDataSource
import com.tokenlab.desafiotecnico.data.TmdbApi
import com.tokenlab.desafiotecnico.util.networkBoundResource
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val api: TmdbApi,
    private val cache: CacheDataSource,
) {

    fun getMovieList() = networkBoundResource(
        query = { cache.getMovieList() },
        fetch = { api.retrofitService.getMovieList() },
        saveFetchResult = { cache.updateMovieList(it) }
    )

    fun getMovieDetails(movieId: Int) = networkBoundResource(
        query = { cache.getMovieDetails(movieId) },
        fetch = { api.retrofitService.getMovieDetails(movieId.toString()) },
        saveFetchResult = { cache.updateMovieDetails(it) },
        shouldFetch = {
            // TODO: Expiration and fetch control logic here
            true
        }
    )
}