package com.tokenlab.desafiotecnico.presenter.moviedetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

/**
 * Simple ViewModel factory that provides the MovieId and context to the ViewModel.
 */
class MovieDetailsViewModelFactory @Inject constructor() :
    ViewModelProvider.Factory {

    companion object {
        private var movieId: Int? = null
        fun setMovieId(id: Int) {
            movieId = id
        }
    }

    private val movieId
        get() = MovieDetailsViewModelFactory.movieId
            ?: throw IllegalAccessException("setMovieId needs to be called first")

    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)) {
            return MovieDetailsViewModel(movieId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}