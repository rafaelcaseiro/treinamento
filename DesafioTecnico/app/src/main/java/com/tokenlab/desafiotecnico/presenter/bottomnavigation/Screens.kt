package com.tokenlab.desafiotecnico.presenter.bottomnavigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.tokenlab.desafiotecnico.presenter.moviedetails.MovieDetailsFragment
import com.tokenlab.desafiotecnico.presenter.movielist.MovieListFragment
import com.tokenlab.desafiotecnico.presenter.secondaryviews.SecondaryOneFragment
import com.tokenlab.desafiotecnico.presenter.secondaryviews.SecondaryTwoFragment

object MovieListFragmentScreen : FragmentScreen {
    override fun createFragment(factory: FragmentFactory): Fragment {
        return MovieListFragment.newInstance()
    }
}

class FragmentMovieDetailsScreen(private val movieId: Int) : FragmentScreen {
    override fun createFragment(factory: FragmentFactory): Fragment {
        return MovieDetailsFragment.newInstance(movieId)
    }
}

object SecondaryOneFragmentScreen : FragmentScreen {
    override fun createFragment(factory: FragmentFactory): Fragment {
        return SecondaryOneFragment.newInstance()
    }
}

object FragmentSecondaryTwoScreen : FragmentScreen {
    override fun createFragment(factory: FragmentFactory): Fragment {
        return SecondaryTwoFragment.newInstance()
    }
}