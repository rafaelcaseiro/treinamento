package com.tokenlab.desafiotecnico.presenter.movielist

import android.view.View
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.databinding.GridViewItemBinding
import com.tokenlab.desafiotecnico.domain.model.Movie
import com.tokenlab.desafiotecnico.util.BindingAdapterSetterFunctions.setImage
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

/**
 * This class implements a [GroupAdapter] [GroupieViewHolder] which uses Data Binding to present
 * a [List] data, including computing diffs between lists, all handled by the Groupie library.
 */
class MovieListAdapter(private val onClickListener: OnClickListener) :
    GroupAdapter<GroupieViewHolder>() {
    /**
     * Associates each [Movie] item from the movie list with an item view from the [MovieItem]
     */
    fun setData(movieList: List<Movie>) {
        val size = movieList.size
        clear()
        notifyItemRangeRemoved(0, size)

        movieList.forEach {
            add(MovieItem(it))
        }
    }

    /**
     * Generates a grid item view, associated with an instance of the [R.layout.grid_view_item]
     * and binds the movie data for the item.
     */
    private inner class MovieItem(private val movie: Movie) :
        BindableItem<GridViewItemBinding>() {

        override fun getLayout(): Int = R.layout.grid_view_item

        override fun bind(viewBinding: GridViewItemBinding, position: Int) {
            viewBinding.movieTitle.text = movie.title
            setImage(viewBinding.moviePosterImage, movie.posterUrl)

            // Set OnCLickListener() using an anonymous class:
            viewBinding.root.setOnClickListener {
                onClickListener.onClick(movie.id)
            }
        }

        override fun initializeViewBinding(view: View): GridViewItemBinding {
            return GridViewItemBinding.bind(view)
        }
    }

    fun interface OnClickListener {
        fun onClick(movieId: Int)
    }
}