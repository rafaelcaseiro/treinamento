package com.tokenlab.desafiotecnico.presenter.bottomnavigation

/**
 * This interface supports the back button actions
 */
interface BackButtonListener {
    fun onBackPressed(): Boolean
}