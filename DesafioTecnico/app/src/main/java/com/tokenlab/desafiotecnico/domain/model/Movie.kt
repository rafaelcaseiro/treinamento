package com.tokenlab.desafiotecnico.domain.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "movie_list")
data class Movie(
    @Json(name = "id") @PrimaryKey val id: Int,
    @Json(name = "vote_average") val voteAverage: Double,
    @Json(name = "title") val title: String,
    @Json(name = "poster_url") val posterUrl: String,
    @Json(name = "release_date") val releaseDate: String?,
    @Json(name = "overview") val overview: String?,
) : Parcelable