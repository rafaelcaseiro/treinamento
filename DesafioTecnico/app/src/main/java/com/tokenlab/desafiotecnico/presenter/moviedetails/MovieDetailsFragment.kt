package com.tokenlab.desafiotecnico.presenter.moviedetails

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.tokenlab.desafiotecnico.databinding.FragmentMovieDetailsBinding
import com.tokenlab.desafiotecnico.domain.model.Movie
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.BackButtonListener
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolder
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolderDelegate
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerFragment
import com.tokenlab.desafiotecnico.util.BindingAdapterSetterFunctions
import javax.inject.Inject

/**
 * This [Fragment] shows the detailed information about a selected movie from the TMDB API REST.
 * It sets this information in the [MovieDetailsViewModel].
 */
class MovieDetailsFragment : Fragment(), BackButtonListener,
    DisposableHolder by DisposableHolderDelegate() {
    companion object {
        fun newInstance(movieId: Int) = MovieDetailsFragment().apply {
            arguments = bundleOf(
                "movieId" to movieId
            )
        }
    }

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var viewModelFactory: MovieDetailsViewModelFactory

    private lateinit var viewModel: MovieDetailsViewModel

    private lateinit var binding: FragmentMovieDetailsBinding

    private val movieDetailsComponent: MovieDetailsComponent by lazy {
        DaggerMovieDetailsComponent
            .builder()
            .fluxContainerComponent((parentFragment as? FluxContainerFragment)?.fluxContainerComponent)
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentMovieDetailsBinding.inflate(layoutInflater, container, false)

        MovieDetailsViewModelFactory.setMovieId(requireArguments().getInt("movieId"))

        movieDetailsComponent.inject(this@MovieDetailsFragment)

        // Get a reference to the ViewModel associated with this fragment.
        viewModel = ViewModelProvider(this, viewModelFactory)[MovieDetailsViewModel::class.java]

        viewModel.liveDataApiStatus.observe(viewLifecycleOwner) {
            BindingAdapterSetterFunctions.setStatus(binding.statusImage, it)
        }

        viewModel.liveDataMovieResource.observe(viewLifecycleOwner) {
            displayMovieDetail(it.data)
        }

        return binding.root
    }

    private fun displayMovieDetail(movie: Movie?) {
        if (movie == null) {
            Log.i("MovieDetailsFragment", "Empty movie loaded!")
            return
        }
        binding.movieTitle.text = movie.title
        binding.movieRelease.text = movie.releaseDate
        binding.movieOverview.text = movie.overview

        BindingAdapterSetterFunctions.setImage(binding.moviePosterImage, movie.posterUrl)
    }

    override fun onBackPressed(): Boolean {
        cicerone.router.exit()
        return true
    }

    override fun onDestroy() {
        disposeAll()
        super.onDestroy()
    }
}