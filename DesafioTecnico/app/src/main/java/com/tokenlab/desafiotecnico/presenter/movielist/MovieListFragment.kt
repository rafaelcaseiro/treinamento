package com.tokenlab.desafiotecnico.presenter.movielist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.tokenlab.desafiotecnico.databinding.FragmentMovieListBinding
import com.tokenlab.desafiotecnico.domain.model.Movie
import com.tokenlab.desafiotecnico.presenter.InitialScreen
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.*
import com.tokenlab.desafiotecnico.util.BindingAdapterSetterFunctions
import javax.inject.Inject

class MovieListFragment : Fragment(), BackButtonListener, InitialScreen,
    DisposableHolder by DisposableHolderDelegate() {
    companion object {
        fun newInstance(): MovieListFragment = MovieListFragment()
    }

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var viewModelFactory: MovieListViewModelFactory

    private val movieListComponent: MovieListComponent by lazy {
        DaggerMovieListComponent
            .builder()
            .fluxContainerComponent((parentFragment as? FluxContainerFragment)?.fluxContainerComponent)
            .build()
    }

    private lateinit var binding: FragmentMovieListBinding

    private lateinit var viewModel: MovieListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentMovieListBinding.inflate(layoutInflater, container, false)

        // Injecting the component of the bottom navigation bar and the factory of the viewModel
        movieListComponent.inject(this)

        // Get a reference to the ViewModel associated with this fragment.
        viewModel = ViewModelProvider(this, viewModelFactory)[MovieListViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.movieListRecyclerView.adapter = MovieListAdapter {
            viewModel.displayMovieDetails(it)
        }

        viewModel.liveDataApiStatus.observe(viewLifecycleOwner) {
            BindingAdapterSetterFunctions.setStatus(binding.statusImage, it)
        }

        viewModel.liveDataMovieResource.observe(viewLifecycleOwner) {
            setRecyclerView(binding.movieListRecyclerView, it.data)
        }

        viewModel.liveDataSelectedMovieId.observe(viewLifecycleOwner) {
            if (null != it) {
                cicerone.router.navigateTo(FragmentMovieDetailsScreen(it))

                // Tell the ViewModel we've made the navigate call to prevent multiple navigation
                viewModel.displayMovieDetailsComplete()
            }
        }
    }

    override fun onBackPressed(): Boolean {
        cicerone.router.exit()
        return true
    }

    override fun onDestroy() {
        disposeAll()
        super.onDestroy()
    }

    /**
     * When there is no TMBD property data (data is null), hide the [RecyclerView], otherwise show it.
     */
    private fun setRecyclerView(recyclerView: RecyclerView, data: List<Movie>?) {
        val adapter = recyclerView.adapter as MovieListAdapter
        if (data != null) {
            adapter.setData(data)
        }
    }
}