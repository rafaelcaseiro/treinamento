package com.tokenlab.desafiotecnico.presenter.movielist

import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerComponent
import com.tokenlab.desafiotecnico.util.PerScreen
import dagger.Component

@PerScreen
@Component(dependencies = [FluxContainerComponent::class])
interface MovieListComponent {
    fun inject(movieListFragment: MovieListFragment)
}