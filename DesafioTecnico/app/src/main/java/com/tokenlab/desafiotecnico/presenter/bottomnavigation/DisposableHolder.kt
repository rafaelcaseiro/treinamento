package com.tokenlab.desafiotecnico.presenter.bottomnavigation

import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

interface DisposableHolder {
    val disposables: CompositeDisposable

    fun disposeAll()
}

class DisposableHolderDelegate @Inject constructor() : DisposableHolder {
    override val disposables: CompositeDisposable by lazy { CompositeDisposable() }

    override fun disposeAll() {
        disposables.clear()
    }
}