package com.tokenlab.desafiotecnico.presenter.bottomnavigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Navigator
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.presenter.InitialScreen
import javax.inject.Inject

/**
 * This class will work as a container for the views to allow the cicerone library
 * to keep their return stack and liveDataApiStatus.
 */
sealed class FluxContainerFragment : Fragment(), BackButtonListener {
    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var cicerone: Cicerone<Router>

    val fluxContainerComponent: FluxContainerComponent? by lazy {
        context?.let {
            DaggerFluxContainerComponent
                .builder()
                .fluxContainerModule((activity)?.let { fragmentActivity ->
                    FluxContainerModule(fragmentActivity, childFragmentManager)
                })
                .build()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        fluxContainerComponent?.inject(this)

        return inflater.inflate(R.layout.fragment_flux_container, container, false)
    }

    override fun onPause() {
        super.onPause()
        this.navigatorHolder.removeNavigator()
    }

    override fun onResume() {
        super.onResume()
        this.navigatorHolder.setNavigator(this.navigator)
    }

    /**
     * If the active fragment implements the [InitialScreen] interface, exit the app.
     * Else just return to the previous screen in the cicerone stack.
     */
    override fun onBackPressed(): Boolean {
        return if (isAdded) {
            val childFragment = childFragmentManager.findFragmentById(R.id.flux_container)
            return if (childFragment is InitialScreen) {
                activity?.finish()
                false
            } else {
                (childFragment as? BackButtonListener)?.onBackPressed()
                true
            }
        } else false
    }
}

class MovieListViewFluxContainer : FluxContainerFragment() {
    companion object {
        fun newInstance() = MovieListViewFluxContainer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            cicerone.router.navigateTo(MovieListFragmentScreen)
        }
    }
}

class SecondaryViewFluxContainer : FluxContainerFragment() {
    companion object {
        fun newInstance() = SecondaryViewFluxContainer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            cicerone.router.navigateTo(SecondaryOneFragmentScreen)
        }
    }
}