package com.tokenlab.desafiotecnico.presenter.moviedetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.tokenlab.desafiotecnico.MovieApplication
import com.tokenlab.desafiotecnico.data.ApiStatus
import com.tokenlab.desafiotecnico.data.DaggerDataServicesComponent
import com.tokenlab.desafiotecnico.domain.repository.MovieRepository
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolder
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolderDelegate
import com.tokenlab.desafiotecnico.util.Resource
import javax.inject.Inject

class MovieDetailsViewModel(movieId: Int) : ViewModel(),
    DisposableHolder by DisposableHolderDelegate() {

    //Injecting the repository that will handle data management
    @Inject
    lateinit var repository: MovieRepository

    // The liveData holding the list of movies to be exposed on the recycler view
    val liveDataMovieResource by lazy { repository.getMovieDetails(movieId).asLiveData() }

    // The internal MutableLiveData that stores the liveDataApiStatus of the most recent request
    private val _mutableApiStatus = MutableLiveData<ApiStatus>()
    // The external immutable LiveData for the request liveDataApiStatus, which exposes it to the binding adapter
    val liveDataApiStatus: LiveData<ApiStatus>
        get(){
            _mutableApiStatus.value = when (liveDataMovieResource.value) {
                is Resource.Loading -> { ApiStatus.LOADING }
                is Resource.Error -> { ApiStatus.ERROR }
                else -> { ApiStatus.DONE }
            }
            return _mutableApiStatus
        }

    init {
        DaggerDataServicesComponent.builder()
            .movieApplicationComponent(MovieApplication.instance.component)
            .build()
            .inject(this@MovieDetailsViewModel)
    }
}