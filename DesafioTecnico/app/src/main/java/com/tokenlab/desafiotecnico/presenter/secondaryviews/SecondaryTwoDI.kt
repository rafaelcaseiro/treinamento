package com.tokenlab.desafiotecnico.presenter.secondaryviews

import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerComponent
import com.tokenlab.desafiotecnico.util.PerScreen
import dagger.Component

@PerScreen
@Component(dependencies = [FluxContainerComponent::class])
interface SecondaryTwoComponent {
    fun inject(secondaryTwoFragment: SecondaryTwoFragment)
}