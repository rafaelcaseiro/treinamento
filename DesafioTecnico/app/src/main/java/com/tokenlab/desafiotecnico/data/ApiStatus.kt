package com.tokenlab.desafiotecnico.data

enum class ApiStatus { LOADING, ERROR, DONE }