package com.tokenlab.desafiotecnico

import android.content.Context
import androidx.room.Room
import com.tokenlab.desafiotecnico.data.MovieDatabase
import com.tokenlab.desafiotecnico.presenter.movielist.MovieListViewModelFactory
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

// If two or more different contexts are needed, than it will be necessary to annotate each one.
@Module
class MovieApplicationModule(private val context: Context) {
    @Singleton
    @Provides
    fun context() = context
}

@Module
class CacheServiceModule {
    @Singleton
    @Provides
    fun provideDatabase(@Singleton context: Context): MovieDatabase =
        Room.databaseBuilder(context, MovieDatabase::class.java, "movie_database")
            .build()
}

@Singleton
@Component(modules = [MovieApplicationModule::class, CacheServiceModule::class])
interface MovieApplicationComponent {
    fun context(): Context
    fun inject(movieListViewModelFactory: MovieListViewModelFactory)
    fun provideDatabase(): MovieDatabase
}