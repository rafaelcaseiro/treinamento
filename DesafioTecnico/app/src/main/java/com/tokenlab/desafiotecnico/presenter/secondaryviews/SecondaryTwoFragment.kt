package com.tokenlab.desafiotecnico.presenter.secondaryviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.BackButtonListener
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolder
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.DisposableHolderDelegate
import com.tokenlab.desafiotecnico.presenter.bottomnavigation.FluxContainerFragment
import javax.inject.Inject

class SecondaryTwoFragment : Fragment(), BackButtonListener,
    DisposableHolder by DisposableHolderDelegate() {
    @Inject
    lateinit var cicerone: Cicerone<Router>

    private val secondaryTwoComponent: SecondaryTwoComponent by lazy {
        DaggerSecondaryTwoComponent
            .builder()
            .fluxContainerComponent((parentFragment as? FluxContainerFragment)?.fluxContainerComponent)
            .build()
    }

    companion object {
        fun newInstance(): SecondaryTwoFragment = SecondaryTwoFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        secondaryTwoComponent.inject(this)

        return inflater.inflate(R.layout.fragment_secondary_two, container, false)
    }

    override fun onBackPressed(): Boolean {
        cicerone.router.exit()
        return true
    }

    override fun onDestroy() {
        disposeAll()
        super.onDestroy()
    }
}