package com.tokenlab.desafiotecnico.data

import com.tokenlab.desafiotecnico.domain.model.Movie
import retrofit2.http.GET
import retrofit2.http.Path
import javax.inject.Inject

/**
 * A public interface that exposes the [getMovieList] and [getMovieDetails] methods
 */
interface TmdbApiService {
    /**
     * Returns a Coroutine [List] of [Movie] which can be fetched in a Coroutine scope.
     * The @GET annotation indicates that the "movies" endpoint will be requested with the GET
     * HTTP method, and the @PATH specifies a variable parameter in the string.
     * Similar for [getMovieDetails], but returning a single [Movie].
     */
    @GET("movies")
    suspend fun getMovieList(): List<Movie>

    @GET("movies/{movieId}")
    suspend fun getMovieDetails(
        @Path("movieId") movieId: String,
    ): Movie
}

/**
 * A public Api object that injects the Retrofit service
 */
class TmdbApi @Inject constructor(val retrofitService: TmdbApiService)