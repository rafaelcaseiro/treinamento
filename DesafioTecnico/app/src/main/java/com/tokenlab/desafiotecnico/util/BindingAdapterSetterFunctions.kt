package com.tokenlab.desafiotecnico.util

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import com.bumptech.glide.request.RequestOptions
import com.tokenlab.desafiotecnico.R
import com.tokenlab.desafiotecnico.data.ApiStatus

object BindingAdapterSetterFunctions {
    /**
     * Uses the Glide library to load an image by URL into an [ImageView]
     */
    fun setImage(imgView: ImageView, imgUrl: String?) {
        imgUrl?.let {
            val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
            GlideApp.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
                .into(imgView)
        }
    }

    /**
     * This binding adapter displays the [ApiStatus] of the network request in an image view.  When
     * the request is loading, it displays a loading_animation.  If the request has an error, it
     * displays a broken image to reflect the connection error.  When the request is finished, it
     * hides the image view.
     */
    fun setStatus(statusImageView: ImageView, status: ApiStatus?) {
        when (status) {
            ApiStatus.LOADING -> {
                statusImageView.visibility = View.VISIBLE
                statusImageView.setImageResource(R.drawable.loading_animation)
                statusImageView.contentDescription = "Poster image is loading."
            }
            ApiStatus.ERROR -> {
                statusImageView.visibility = View.VISIBLE
                statusImageView.setImageResource(R.drawable.ic_connection_error)
                statusImageView.contentDescription =
                    "An error occurred while loading the poster image. Check your internet connection and try again."
            }
            ApiStatus.DONE -> {
                statusImageView.visibility = View.GONE
            }
            else -> Log.i("BAdapterSetterFunctions", "Unidentified liveDataApiStatus: $status")
        }
    }
}
