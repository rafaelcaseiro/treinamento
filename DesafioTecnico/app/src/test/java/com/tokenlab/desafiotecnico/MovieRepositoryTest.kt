package com.tokenlab.desafiotecnico

import com.tokenlab.desafiotecnico.data.CacheDataSource
import com.tokenlab.desafiotecnico.data.TmdbApi
import com.tokenlab.desafiotecnico.domain.model.Movie
import com.tokenlab.desafiotecnico.domain.repository.MovieRepository
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class MovieRepositoryTest {
    private val apiMock: TmdbApi = TmdbApi(mockk(relaxed = true))
    private val cacheMock: CacheDataSource = mockk(relaxed = true)
    private val movieRepository: MovieRepository = MovieRepository(apiMock, cacheMock)

    private val movieSample = Movie(13,
        8.4,
        "Forrest Gump",
        "https://image.tmdb.org/t/p/w200/yE5d3BUhE8hCnkMUJOo1QDoOGNz.jpg",
        null,
        null)
    private val movieSampleDetails = Movie(13,
        8.4,
        "Forrest Gump",
        "https://image.tmdb.org/t/p/w200/yE5d3BUhE8hCnkMUJOo1QDoOGNz.jpg",
        "2018-06-30",
        "Forrest Gump is a story.")

    private val movieList: List<Movie> = listOf(movieSample)
    private val movieList2: List<Movie> = listOf(movieSample, movieSample)

    @Test
    fun `when successfully gets movieList from remote data source, updates the cache data source`() {
        // given
        every { cacheMock.getMovieList() } returnsMany listOf(flowOf(movieList), flowOf(movieList2))
        coEvery { cacheMock.updateMovieList(movieList2) } returns Unit
        coEvery { apiMock.retrofitService.getMovieList() } returns movieList2

        // when (test)
        runBlocking {
            val result = movieRepository.getMovieList().drop(1).first()

            // then
            Assertions.assertEquals(2, result.data!!.size)
        }
    }

    @Test
    fun `tries to get movieList from remote data source first and if it fails, tries to get from cache data source`() {
        // given
        every { cacheMock.getMovieList() } returnsMany listOf(flowOf(movieList), flowOf(movieList))
        coEvery { cacheMock.updateMovieList(movieList) } returns Unit
        coEvery { apiMock.retrofitService.getMovieList() } returns emptyList() andThenThrows Exception(
            "Api failure test.")

        // when (test)
        runBlocking {
            val result = movieRepository.getMovieList().take(2).toList()

            // then
            Assertions.assertEquals(movieList, result[1].data)
        }
    }

    @Test
    fun `when successfully gets movieDetails from remote data source, updates the cache data source`() {
        // given
        every { cacheMock.getMovieDetails(movieSample.id) } returnsMany listOf(flowOf(movieSample),
            flowOf(movieSampleDetails))
        coEvery { cacheMock.updateMovieDetails(movieSampleDetails) } returns Unit
        coEvery { apiMock.retrofitService.getMovieDetails(movieSample.id.toString()) } returns movieSampleDetails

        // when (test)
        runBlocking {
            val result = movieRepository.getMovieDetails(movieSample.id).drop(1).first()

            // then
            Assertions.assertEquals(movieSampleDetails, result.data)
        }
    }

    @Test
    fun `tries to get movie details from remote data source first and if it fails, tries to get from cache data source`() {
        val movieErrorSample = Movie(0, 0.0, "", "", null, null)
        // given
        every { cacheMock.getMovieDetails(movieSample.id) } returnsMany listOf(flowOf(movieSample),
            flowOf(movieSample))
        coEvery { cacheMock.updateMovieDetails(movieSampleDetails) } returns Unit
        coEvery { apiMock.retrofitService.getMovieDetails(movieSample.id.toString()) } returns movieErrorSample andThenThrows Exception(
            "Api failure test.")

        // when (test)
        runBlocking {
            val result = movieRepository.getMovieDetails(movieSample.id).drop(1).first()

            // then
            Assertions.assertEquals(movieSample, result.data)
        }
    }
}